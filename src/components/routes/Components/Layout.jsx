import Header from "../../Header/Header";
import {Outlet} from "react-router-dom";
import {useEffect, useState} from "react";
import Modal from "../../Modal/Modal";
import Button from "../../Button/Button";

function Layout() {
    const [products, setProducts] = useState([])
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem("basket")) || [])
    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem("favorite")) || [])
    const [modal, setModal] = useState({type: null, id: null})

    const handleFavourite = (id) => {
        let nextState
        if (favorite.includes(id)
        ) {

            nextState = favorite.filter(productId => productId !== id)
        } else {
            nextState = [...favorite, id]
        }


        setFavorite(nextState)


        localStorage.setItem("favorite", JSON.stringify(nextState))
    }

    const addToBasket = (id) => {
        if (basket.includes(id)) return
        const nextState = [...basket, id]

        setBasket(nextState)
        localStorage.setItem("basket", JSON.stringify(nextState))

    }
    const changeModal = (id, type) => {
        setModal({type, id})
    }
    const removeItemCard = (id) => {
        setBasket(prev => {
            const newArray = prev.filter(element => element !== id)
            localStorage.setItem("basket", JSON.stringify(newArray))
            return newArray
        })
    }
    useEffect(() => {
        fetch("./product.json").then((res) => res.json())
            .then((data) => {
                setProducts(data)
            })
    }, [])

    return (
        <div>
            <Header favLength={favorite.length} cardLength={basket.length}/>
            <Outlet context={{products, favorite, basket, handleFavourite, changeModal}}/>
            {modal.type === "add" &&
                <Modal click={changeModal} header={"Modal add"} closeButton={true}
                       text={"Add product to card?"}
                       actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                                                          click={() => {
                                                                              addToBasket(modal.id)
                                                                              changeModal()
                                                                          }}/><Button text={"Cancel"}
                                                                                      bgColor={"green"}
                                                                                      click={changeModal}/>
                       </div>}/>}
            {modal.type === "delete" &&
                <Modal click={changeModal} header={"Delete modal?"} closeButton={true}
                       text={"Delete this product?"}
                       actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                                                          click={() => {

                                                                              removeItemCard(modal.id)
                                                                              changeModal()
                                                                          }}/><Button text={"Cancel"}
                                                                                      bgColor={"green"}
                                                                                      click={changeModal}/>
                       </div>}/>}
        </div>
    );
}

export default Layout;