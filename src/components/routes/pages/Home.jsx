import  {useOutletContext} from 'react-router-dom';
import List from "../../list/List";
function Home() {
    const {products,favorite,handleFavourite,changeModal}=useOutletContext();
    return (
        <div>
            {products.length &&
                <List favorite={favorite} handleFavorite={handleFavourite} handleClick={changeModal} data={products}/>}

        </div>
    );
}
export default Home;