import {useOutletContext} from "react-router-dom";
import List from "../../list/List";

function Favourite() {
    const {products, favorite, handleFavourite, changeModal} = useOutletContext();
    return (
        <div>
            {favorite.length &&
                <List favorite={favorite} handleFavorite={handleFavourite} handleClick={changeModal}
                      data={products.filter(product =>favorite.includes(product.id))}/>}

        </div>
    );
}

export default Favourite;