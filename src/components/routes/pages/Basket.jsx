import {useOutletContext} from "react-router-dom";
import List from "../../list/List";

function Basket() {
    const {products, favorite,basket, handleFavourite, handleClick, changeModal} = useOutletContext();
    return (
        <div>
            {basket.length &&
                <List favorite={favorite} handleFavorite={handleFavourite} cardClick={changeModal} handleClick={handleClick}
                      data={products.filter(product =>basket.includes(product.id))}/>}

        </div>
    );
}
export default Basket;