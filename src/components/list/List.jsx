import Card from "../card/Card";
import PropTypes from "prop-types";
import styles from "./List.module.css"

function List ({data, handleClick, handleFavorite, favorite,cardClick}) {
        return <ul className={styles.wrapper}>{data.map((product) => <Card key={product.id} product={product}
                                                                           cardClick={cardClick}
                                                                           isFavorite={favorite.includes(product.id)}
                                                                           handleFavorite={() => handleFavorite(product.id)}
                                                                           handleClick={handleClick}/>)}</ul>

}

List.propTypes = {
    data: PropTypes.array,
    handleClick: PropTypes.func,
    handleFavorite: PropTypes.func,
    favorite: PropTypes.array
}
export default List;
