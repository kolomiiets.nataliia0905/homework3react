import Button from "../Button/Button";
import {ReactComponent as StarIcon} from "../../img/star-fill.svg"
import PropTypes from "prop-types";
import styles from "./Card.module.css"

function Card({handleClick, product, handleFavorite, isFavorite, cardClick}) {
    const {name, price, image, article,id} = product
    const starColor = isFavorite ? "red" : "black"
    const showButton = cardClick? <Button text={"Delete product (X)"} click={()=>cardClick(id,"delete")}/>:<Button click={()=>handleClick(id, "add")} text={"Add to cart"}/>
    return <li className={styles.card}><StarIcon className={styles.star} style={{fill: starColor}}
                                                 onClick={handleFavorite}/><img className={styles.img}
                                                                                         src={image}
                                                                                         alt={name}/>
        <div><h3>{name}</h3><h4>{article}</h4><h4>{price}</h4>{showButton}
        </div>
    </li>
}

Card.propTypes = {
    handleClick: PropTypes.func,
    product: PropTypes.object,
    handleFavorite: PropTypes.func,
    isFavorite: PropTypes.bool
}

export default Card;
